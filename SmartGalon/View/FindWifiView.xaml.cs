﻿using SmartGalon.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices.WiFi;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Security.Credentials;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Devices.WiFiDirect;
using Windows.UI.Popups;
using SmartGalon.Helper;
using SmartGalon.Model;
using System.Threading.Tasks;
using Newtonsoft.Json;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SmartGalon.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FindWifiView : Page
    {
        private WiFiAdapter firstAdapter;
        private FindWifiViewModel vm;
        public FindWifiView()
        {
            this.InitializeComponent();
            vm = new FindWifiViewModel();
            DataContext = vm;
            vm.PropertyChanged += Vm_PropertyChanged;
            Back.Click += BackButton_Click;
        }

        private async void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Error")
            {
                try
                {
                    var dialog = new Windows.UI.Popups.MessageDialog(vm.Error, e.PropertyName);
                    await dialog.ShowAsync();
                }
                catch { }

            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            firstAdapter.Disconnect();
            Frame.GoBack();
        }
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!Frame.CanGoBack)
            {
                Back.Visibility = Visibility.Collapsed;
            }
            RefreshWifiList();
        }



        private void OnConnectionChanged(WiFiDirectDevice sender, object args)
        {
            //throw new NotImplementedException();
        }

        private async void connect(WiFiAvailableNetwork wifi, PasswordCredential credential = null)
        {
            vm.Progress = Visibility.Visible;
            WiFiConnectionResult result;
            var reconnectionKind = WiFiReconnectionKind.Automatic;
            if (credential == null)
            {
                result = await firstAdapter.ConnectAsync(wifi, reconnectionKind);
            }
            else
            {
                result = await firstAdapter.ConnectAsync(wifi, reconnectionKind, credential);
            }

            if (result.ConnectionStatus == WiFiConnectionStatus.Success)
            {
               
                Debug.Write("");
                try
                {
                    //var dialog = new Windows.UI.Popups.MessageDialog("Connected", wifi.Ssid);
                    //await dialog.ShowAsync();
                    var currentWifi = new WifiAccountModel
                    {
                        wifi = wifi,
                        credential = credential
                    };
                    firstAdapter.Disconnect();
                    this.Frame.Navigate(typeof(FindDeviceView), currentWifi);
                }
                catch { }
                Debug.Write("");
            }
            else
            {
                vm.Error = "Wrong Credential";
                //rootPage.NotifyUser(string.Format("Could not connect to {0}. Error: {1}", selectedNetwork.Ssid, result.ConnectionStatus), NotifyType.ErrorMessage);
            }
            vm.ProgressValue = 100;

            vm.Progress = Visibility.Collapsed;
        }

        private async void ConnectToWifi(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var wifi = (WiFiAvailableNetwork)button.Tag;

            if (wifi.SecuritySettings.NetworkAuthenticationType == Windows.Networking.Connectivity.NetworkAuthenticationType.Open80211)
            {
                //result = await firstAdapter.ConnectAsync(wifi, reconnectionKind);
                connect(wifi);
            }
            else
            {
                // Only the password potion of the credential need to be supplied
                //var credential = new PasswordCredential();
                //credential.Password = "utbuutbu";

                //result = await firstAdapter.ConnectAsync(wifi, reconnectionKind, credential);
                ContentDialog dialog = new ContentDialog();
                dialog.Title = wifi.Ssid;
                dialog.Tag = wifi;
                var passBox = new PasswordBox();
                passBox.PlaceholderText = "Password";
                //passBox.Password = "957DaegCen";
                dialog.IsPrimaryButtonEnabled = true;
                dialog.IsSecondaryButtonEnabled = true;
                dialog.PrimaryButtonClick += ConnectDialog_PrimaryButtonClick;
                dialog.SecondaryButtonClick += ConnectDialog_SecondaryButtonClick;
                dialog.Content = passBox;
                dialog.PrimaryButtonText = "Connect";
                dialog.SecondaryButtonText = "Cancel";
                await dialog.ShowAsync();
            }
        }

        private void ConnectDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
        }

        private void ConnectDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var wifi = (WiFiAvailableNetwork)sender.Tag;
            var passBox = (PasswordBox)sender.Content;
            var password = passBox.Password;

            var credential = new PasswordCredential();
            credential.Password = password;

            sender.Hide();
            connect(wifi, credential);
        }

        private async void RefreshWifiList()
        {
            vm.IsLoading = true;
            try
            {
                firstAdapter.Disconnect();
            }
            catch { }
            var access = await WiFiAdapter.RequestAccessAsync();
            if (access != WiFiAccessStatus.Allowed)
            {
                var dialog = new Windows.UI.Popups.MessageDialog("wifi access not allowed", "error");
                await dialog.ShowAsync();
            }
            else
            {
                try
                {
                    //var deviceSelector = Windows.Devices.WiFi.WiFiAdapter.GetDeviceSelector();
                    var result = await Windows.Devices.WiFi.WiFiAdapter.FindAllAdaptersAsync();
                    //var result = await Windows.Devices.Enumeration.DeviceInformation.FindAllAsync(deviceSelector);
                    if (result.Count >= 1)
                    {
                        try
                        {
                            firstAdapter = result.First();// await WiFiAdapter.FromIdAsync(result[0].Id);
                            await firstAdapter.ScanAsync();
                            var report = firstAdapter.NetworkReport;
                            //foreach (var network in report.AvailableNetworks)
                            //{
                            //    vm.NetWorks.Add(network);
                            //    //Debug.Write(network);
                            //}
                            var availableNetwork = report.AvailableNetworks.Where(x => !x.Ssid.ToLower().Contains(ConfigHelper.DeviceName)).ToList();
                            var networks = new Dictionary<string, WiFiAvailableNetwork>();

                            foreach (var an in availableNetwork)
                            {
                                networks.Add(an.Ssid, an);
                            }
                            vm.NetWorks = networks;
                        }
                        catch (Exception ex)
                        {
                            try { firstAdapter.Disconnect(); } catch { }

                            vm.Error = ex.Message;
                        }
                    }
                    else
                    {
                        var dialog = new Windows.UI.Popups.MessageDialog("No WiFi Adapters detected on this machine", "error"); await dialog.ShowAsync();
                        //rootPage.NotifyUser("No WiFi Adapters detected on this machine", NotifyType.ErrorMessage);
                        await dialog.ShowAsync();
                    }
                }
                catch (Exception ex)
                {
                    vm.Error = ex.Message;
                }


            }
            vm.IsLoading = false;
        }
        private async void RefreshWifi(object sender, RoutedEventArgs e)
        {
            RefreshWifiList();
        }
    }
}
