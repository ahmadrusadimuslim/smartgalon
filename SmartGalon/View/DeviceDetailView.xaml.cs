﻿using Newtonsoft.Json;
using SmartGalon.Helper;
using SmartGalon.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SmartGalon.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DeviceDetailView : Page
    {
        private string username;
        private string deviceId;
        private DeviceDetailViewModel vm;
        public DeviceDetailView()
        {
            
            this.InitializeComponent();
            vm = new DeviceDetailViewModel();
            DataContext = vm;
            vm.PropertyChanged += Vm_PropertyChanged;
            Windows.UI.Core.SystemNavigationManager.GetForCurrentView().BackRequested += (s, a) =>
            {
                //Debug.WriteLine("BackRequested");
                //if (Frame.CanGoBack)
                //{
                //    Frame.GoBack();
                //    a.Handled = true;
                //}
            };
            Back.Click += BackButton_Click;

        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
        private async void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Error")
            {
                try
                {
                    var dialog = new Windows.UI.Popups.MessageDialog(vm.Error, e.PropertyName);
                    await dialog.ShowAsync();
                }
                catch { }

            }
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var totalBackStack = this.Frame.BackStack.Count;
            for (var i = totalBackStack - 1; i >= 0; i--)
            {
                if (i == 0)
                {
                    continue;
                }
                this.Frame.BackStack.RemoveAt(i);
            }
            if (!Frame.CanGoBack)
            {
                Back.Visibility = Visibility.Collapsed;
            }
            var data = e.Parameter as Model.UserDevicesModel.Datum;
            Debug.Write("");

            this.username = ConfigHelper.Login.username;
            this.deviceId = data.device.id;

            vm.SubscribeMetric(deviceId, username);
            vm.SubscribeGetState(deviceId, username);
            vm.SetInitialData(data);
            base.OnNavigatedTo(e);
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                vm.DisconnectSocket();
                DeviceDetailViewModel.PageCache = "";
            }
            else
            {
                vm.GalonImage = null;
                vm.data.attr.GalonImage = null;
                DeviceDetailViewModel.PageCache = JsonConvert.SerializeObject(vm);
            }
            base.OnNavigatedFrom(e);
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //var device = (Model.UserDevicesModel.Device)e.OriginalSource;

        }

        private void StateClicked(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var state = int.Parse(button.Tag.ToString());
            state = state == 1 ? 0 : 1;
            var data = new StatePayload { state = state, deviceId=deviceId };
            vm.Publish(deviceId, JsonConvert.SerializeObject(data), username);
            //vm.data.attr.state = state;
            //vm.State = state;
        }

        private void TriggerClicked(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            Debug.Write("");
            this.Frame.Navigate(typeof(TriggerView), vm.data);
        }
    }
}
