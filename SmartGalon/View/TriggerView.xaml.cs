﻿using SmartGalon.Helper;
using SmartGalon.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SmartGalon.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TriggerView : Page
    {
        private string username;
        private string deviceid;
        private TrigerViewModel vm;
        public TriggerView()
        {
            vm = new TrigerViewModel();
            this.InitializeComponent();
            vm.PropertyChanged += Vm_PropertyChanged;
            DataContext = vm;
            Back.Click += BackButton_Click;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }

        private async void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Error")
            {
                try
                {
                    var dialog = new Windows.UI.Popups.MessageDialog(vm.Error, e.PropertyName);
                    await dialog.ShowAsync();
                }
                catch { }

            }
        }
        private void Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            var value = (int)(e.NewValue);
            var div = value / 10;
            var mod = value % 10;
            vm.WaterTreshold = mod < 5 ? (div * 10) : ((div * 10) + 10);
        }
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!Frame.CanGoBack)
            {
                Back.Visibility = Visibility.Collapsed;
            }
            var data = e.Parameter as Model.UserDevicesModel.Datum;
            Debug.Write("");

            this.username = ConfigHelper.Login.username;
            this.deviceid = data.device.id;

            vm.SetInitialData(data);

            if (e.NavigationMode == NavigationMode.Back)
            {
                var temp = vm.ContactData;
                vm.ContactData = null;
                vm.ContactData = temp;
            }

            base.OnNavigatedTo(e);
        }
        protected override async void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                vm.SaveChange();
            }
            base.OnNavigatedFrom(e);
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var contact = (Model.TriggerModel.Action)button.Tag;

            var helper = new HttpHelper();
            bool result = helper.Delete(EndPointHelper.HOST + EndPointHelper.TRIGGER + "/" + contact._id);

            if (result)
            {
                var item = vm.ContactData.Where(x => x.to == contact.to).First();
                vm.ContactData.Remove(item);
                var temp = vm.ContactData;
                vm.ContactData = null;
                vm.ContactData = temp;
            }
            else
            {
                vm.Error = "Fail to remove contact";
            }
            
        }

        private void AddContact_Clicked(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(AddContactView), new KeyValuePair<Model.UserDevicesModel.Datum, List<Model.TriggerModel.Action>>(vm.data, vm.ContactData));
        }
    }
}
