﻿using Newtonsoft.Json;
using SmartGalon.Helper;
using SmartGalon.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Windows;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SmartGalon.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginView : Page
    {
        private LoginViewModel vm;
        public LoginView()
        {
            vm = new LoginViewModel();
            this.InitializeComponent();
            DataContext = vm;
            vm.PropertyChanged += Vm_PropertyChanged;
            vm.Username = "stub2";
            vm.Password = "stub2";
        }

        private async void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Error")
            {
                try
                {
                    var dialog = new Windows.UI.Popups.MessageDialog(vm.Error, e.PropertyName);
                    await dialog.ShowAsync();
                }
                catch { }
                
            }
        }

        private async void Login_Clicked(object sender, RoutedEventArgs e)
        {
            vm.IsLoading = true;
            var username = vm.Username;
            var password = vm.Password;
            var loginModel = await vm.Authenticate(username, password);
            if (loginModel != null)
            {
                ConfigHelper.Login = loginModel;
                this.Frame.Navigate(typeof(UserDevicesView));
            }
            else
            {

                //fail login
                vm.Error = "Login Failed";
            }


            if (username == "testing" && password == "testing")
            {
                ConfigHelper.Login = new Model.LoginModel.RootObject
                {
                    access_token = "testing",
                    expires_in = DateTime.Now,
                    refresh_token = "testing",
                    username = "testing",
                    token_type = "testing"
                };
                this.Frame.Navigate(typeof(UserDevicesView));
            }

            vm.IsLoading = false;
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            var fileHelper = new FileHelper();
            string findIncache = await fileHelper.GetAuth();

            if (findIncache != null)
            {
                var auth = JsonConvert.DeserializeObject<Model.LoginModel.RootObject>(findIncache);
                if (auth.expires_in > DateTime.Now && !string.IsNullOrEmpty(auth.username))
                {
                    ConfigHelper.Login = auth;
                    this.Frame.Navigate(typeof(UserDevicesView));
                }
            }
        }
    }
}
