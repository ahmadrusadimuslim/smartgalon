﻿using SmartGalon.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SmartGalon.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class UserDevicesView : Page
    {
        private UserDevicesViewModel vm;
        public UserDevicesView()
        {
            this.InitializeComponent();
            vm = new UserDevicesViewModel();
            DataContext = vm;
            vm.PropertyChanged += Vm_PropertyChanged;
            //Windows.UI.Core.SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            Windows.UI.Core.SystemNavigationManager.GetForCurrentView().BackRequested += (s, a) =>
            {
                Debug.WriteLine("BackRequested");
                if (Frame.CanGoBack)
                {
                    Frame.GoBack();
                    a.Handled = true;
                }
                else
                {
                    //CoreApplication.Exit();
                }
            };
            Back.Click += BackButton_Click;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }
        private async void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Error")
            {
                try
                {
                    var dialog = new Windows.UI.Popups.MessageDialog(vm.Error, e.PropertyName);
                    await dialog.ShowAsync();
                }
                catch { }

            }
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            vm.LoadDevices();
            vm.SubscribeMetric();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Frame.BackStack.Clear();
            if (!Frame.CanGoBack)
            {
                Back.Visibility = Visibility.Collapsed;
            }
        }
        private void DeviceClicked(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var device = (Model.UserDevicesModel.Datum)button.Tag;
            Debug.Write("");
            this.Frame.Navigate(typeof(DeviceDetailView), device);
        }

        private void DeviceClick(object sender, RoutedEventArgs e)
        {
            //var button = (Button)sender;
            //var device = (Model.UserDevicesModel.Datum)button.Tag;
            Debug.Write("");
            this.Frame.Navigate(typeof(FindWifiView), null);
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            vm.LoadDevices();
            vm.SubscribeMetric();
        }
    }
}
