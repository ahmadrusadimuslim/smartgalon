﻿using SmartGalon.Helper;
using SmartGalon.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SmartGalon.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddContactView : Page
    {
        private string username;
        private string deviceId;
        private AddContactViewModel vm;
        public AddContactView()
        {
            vm = new AddContactViewModel();
            vm.PropertyChanged += Vm_PropertyChanged;
            this.InitializeComponent();
            DataContext = vm;
            Back.Click += BackButton_Click;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.GoBack();
        }


        private async void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Error")
            {
                try
                {
                    var dialog = new Windows.UI.Popups.MessageDialog(vm.Error, e.PropertyName);
                    await dialog.ShowAsync();
                }
                catch { }

            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!Frame.CanGoBack)
            {
                Back.Visibility = Visibility.Collapsed;
            }
            var data = e.Parameter as Nullable<KeyValuePair <Model.UserDevicesModel.Datum,List<Model.TriggerModel.Action>>>??new KeyValuePair<Model.UserDevicesModel.Datum, List<Model.TriggerModel.Action>>();
            Debug.Write("");

            this.username = ConfigHelper.Login.username;
            this.deviceId = data.Key.device.id;
            vm.LoadData(data.Key, data.Value);
            base.OnNavigatedTo(e);
        }

        private void SubmitClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (vm.SubmitContact())
                {
                    this.Frame.GoBack();
                }
                else
                {
                    vm.Error = "Fail to add contact";
                }
            }
            catch (Exception ex)
            {
                vm.Error = ex.Message;
            }
        }
    }
}
