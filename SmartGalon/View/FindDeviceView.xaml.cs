﻿using Newtonsoft.Json;
using SmartGalon.Helper;
using SmartGalon.Model;
using SmartGalon.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.WiFi;
using Windows.Devices.WiFiDirect;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.Connectivity;
using Windows.Security.Credentials;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SmartGalon.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FindDeviceView : Page
    {
        private WiFiAdapter firstAdapter;
        private FindWifiViewModel vm;
        public FindDeviceView()
        {
            this.InitializeComponent();
            vm = new FindWifiViewModel();
            DataContext = vm;
            vm.PropertyChanged += Vm_PropertyChanged;
            Back.Click += BackButton_Click;
        }

        private async void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Error")
            {
                try
                {
                    var dialog = new Windows.UI.Popups.MessageDialog(vm.Error, e.PropertyName);
                    await dialog.ShowAsync();
                }
                catch { }

            }
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            firstAdapter.Disconnect();
            Frame.GoBack();
        }
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!Frame.CanGoBack)
            {
                Back.Visibility = Visibility.Collapsed;
            }
            var param = (WifiAccountModel)e.Parameter;
            vm.PrevWifi = param;
            RefreshWifiList();
        }



        private void OnConnectionChanged(WiFiDirectDevice sender, object args)
        {
            //throw new NotImplementedException();
        }

        private async void connect(WiFiAvailableNetwork wifi, string deviceName, PasswordCredential credential = null)
        {
            vm.Progress = Visibility.Visible;
            try
            {
                WiFiConnectionResult result;
                var reconnectionKind = WiFiReconnectionKind.Manual;
                bool connected = false;

                if (vm.PrevWifi.credential == null)
                {
                    result = await firstAdapter.ConnectAsync(vm.PrevWifi.wifi, reconnectionKind);
                }
                else
                {
                    result = await firstAdapter.ConnectAsync(vm.PrevWifi.wifi, reconnectionKind, vm.PrevWifi.credential);
                }

                connected = result.ConnectionStatus == WiFiConnectionStatus.Success;
                if (!connected)
                {
                    throw new Exception("Fail to connect wifi");
                }

                vm.ProgressMessage = "Connecting to " + vm.PrevWifi.wifi.Ssid;
                vm.ProgressValue = 30;
                await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);
                vm.ProgressValue = 60;
                vm.ProgressMessage = "Checking internet connection";
                await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);
                //connect to iot hub
                vm.ProgressValue = 80;
                vm.ProgressMessage = "Checking server";
                //var url = EndPointHelper.HOST + EndPointHelper.IOT_HUB + "MKR1000-1";
                var url = EndPointHelper.HOST + EndPointHelper.IOT_HUB + wifi.Ssid;
                Debug.WriteLine("get " + url);
                var response = new HttpHelper().Get(url);
                var authParameter = JsonConvert.DeserializeObject<Dictionary<string, string>>(response);

                if (authParameter == null || authParameter.Count == 0)
                {
                    throw new Exception("this wifi could not connect to internet");
                }
                vm.ProgressValue = 100;
                response = null;
                result = null;

                vm.ProgressMessage = "Connecting to device";
                vm.ProgressValue = 10;

                var deviceConnected = firstAdapter.NetworkReport.AvailableNetworks.Where(x => x.Ssid == wifi.Ssid);

                if (false) //(firstAdapter!=null&&  deviceConnected.Count() > 0)
                {
                    // do not reconnect
                    connected = true;
                }
                else
                {
                    if (credential == null)
                    {
                        result = await firstAdapter.ConnectAsync(wifi, reconnectionKind);
                    }
                    else
                    {
                        result = await firstAdapter.ConnectAsync(wifi, reconnectionKind, credential);
                    }
                    connected = (result.ConnectionStatus == WiFiConnectionStatus.Success);
                }



                if (connected)
                {
                    vm.ProgressMessage = "Initializing";
                    vm.ProgressValue = 15;
                    await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);

                    vm.ProgressMessage = "Establishing connection";
                    vm.ProgressValue = 40;
                    //connect to IOT Device COde.
                    var httpHelper = new HttpHelper();

                    DeviceSetupResponse setupResponse;
                    //reconnect to previous wifi
                    if (ConfigHelper.DeviceName == "utbu")
                    {

                        await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);
                        vm.ProgressMessage = "Configuring device";
                        vm.ProgressValue = 60;

                        setupResponse = new DeviceSetupResponse { id = Guid.NewGuid().ToString() };
                        url = "http://www.google.com";
                        response = httpHelper.GetNoAuth(url);

                        if (vm.PrevWifi.credential != null)
                        {
                            result = await firstAdapter.ConnectAsync(vm.PrevWifi.wifi, reconnectionKind, vm.PrevWifi.credential);
                        }
                        else
                        {
                            result = await firstAdapter.ConnectAsync(vm.PrevWifi.wifi, reconnectionKind);
                        }
                        //await Task.Delay(TimeSpan.FromSeconds(15));
                    }
                    else
                    {
                        await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);
                        vm.ProgressMessage = "Configuring device";
                        vm.ProgressValue = 60;





                        var parameter = "";
                        foreach (var paramAuth in authParameter)
                        {
                            
                            
                            parameter += "&" + paramAuth.Key + "=" + WebUtility.UrlEncode(paramAuth.Value);
                        }
                        await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);
                        if (vm.PrevWifi.credential != null)
                        {

                            url = ConfigHelper.DeviceSetupUrl + "?ssidName=" + vm.PrevWifi.wifi.Ssid + "&ssidPass=" + vm.PrevWifi.credential.Password + parameter;
                            Debug.WriteLine("get " + url);
                            response = httpHelper.GetNoAuth(url);

                            result = await firstAdapter.ConnectAsync(vm.PrevWifi.wifi, reconnectionKind, vm.PrevWifi.credential);
                        }
                        else
                        {
                            url = ConfigHelper.DeviceSetupUrl + "?ssidName=" + vm.PrevWifi.wifi.Ssid + parameter;
                            response = httpHelper.GetNoAuth(url);

                            result = await firstAdapter.ConnectAsync(vm.PrevWifi.wifi, reconnectionKind);
                        }
                        if (response.Contains("</html>"))
                        {
                            throw new Exception("Fail to setup deivce");
                        }
                        setupResponse = JsonConvert.DeserializeObject<DeviceSetupResponse>(response);
                        if (setupResponse.id == null)
                        {
                            throw new Exception("Fail to setup deivce");
                        }
                        else
                        {
                            vm.CachedDevice.Add(wifi.Ssid, setupResponse.id);
                            vm.NetWorks.Remove(wifi.Ssid);
                            vm.NetWorks.Add(wifi.Ssid, null);
                            var temp = vm.NetWorks;
                            vm.NetWorks = null;
                            vm.NetWorks = temp;

                            vm.NetWorksSsid = null;
                            vm.NetWorksSsid = vm.NetWorks.Select(x => x.Key).ToList();
                        }
                    }

                    //Dapatkan device id kemudian masuk ke halaman device detail
                    //Issue gak ada api untuk device detail
                    vm.ProgressMessage = "Connecting to network";
                    vm.ProgressValue = 75;
                    await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);
                    var userDeviceVm = new UserDevicesViewModel();
                    vm.ProgressMessage = "Registering device " + deviceName;
                    vm.ProgressValue = 90;
                    await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);

                    //code register device

                    var deviceDetail = vm.RegisterDevice(setupResponse.id, deviceName, vm.PrevWifi.wifi.Ssid);

                    //await dialog.ShowAsync();

                    //=================
                    vm.ProgressMessage = "Complete";
                    vm.ProgressValue = 100;
                    //firstAdapter.Disconnect();


                    this.Frame.Navigate(typeof(DeviceDetailView), deviceDetail);
                }
                else
                {

                    vm.Error = "Wrong Credential";
                    //firstAdapter.Disconnect();
                    //rootPage.NotifyUser(string.Format("Could not connect to {0}. Error: {1}", selectedNetwork.Ssid, result.ConnectionStatus), NotifyType.ErrorMessage);
                }
            }
            catch (Exception ex)
            {
                vm.Error = ex.Message;
                //firstAdapter.Disconnect();
            }

            vm.Progress = Visibility.Collapsed;
            vm.ProgressMessage = "";
            vm.ProgressValue = 0;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var tBox = (TextBox)sender;
            var dialog = (ContentDialog)tBox.Tag;
            if (string.IsNullOrWhiteSpace(tBox.Text))
            {
                dialog.IsPrimaryButtonEnabled = false;
            }
            else
            {
                dialog.IsPrimaryButtonEnabled = true;
            }
        }



        private async void ConnectToWifi(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var ssid = (string)button.Tag;
            try
            {
                var wifi = vm.NetWorks.Where(x => x.Key == ssid).First().Value;
                ContentDialog dialog = new ContentDialog();
                dialog.Title = ssid;
                dialog.Tag = ssid;

                dialog.PrimaryButtonClick += ConnectDialog_PrimaryButtonClick;
                dialog.SecondaryButtonClick += ConnectDialog_SecondaryButtonClick;

                dialog.PrimaryButtonText = "Connect";
                dialog.SecondaryButtonText = "Cancel";
                dialog.IsPrimaryButtonEnabled = true;
                dialog.IsSecondaryButtonEnabled = true;
                dialog.IsPrimaryButtonEnabled = false;

                var textBox = new TextBox();
                textBox.TextChanged += TextBox_TextChanged;
                textBox.Tag = dialog;
                textBox.PlaceholderText = "New device name";

                var panel = new StackPanel();
                panel.Children.Add(textBox);
                dialog.Content = panel;

                if (wifi.SecuritySettings.NetworkAuthenticationType == Windows.Networking.Connectivity.NetworkAuthenticationType.Open80211)
                {
                    //result = await firstAdapter.ConnectAsync(wifi, reconnectionKind);
                    //connect(wifi);
                    await dialog.ShowAsync();
                }
                else
                {
                    var passBox = new PasswordBox();
                    passBox.PlaceholderText = "Wifi password";
                    panel.Children.Add(passBox);

                    await dialog.ShowAsync();
                }
            }
            catch
            {
                //from cache
                ContentDialog dialog = new ContentDialog();
                dialog.Title = ssid;
                dialog.Tag = ssid;

                dialog.PrimaryButtonClick += ConnectDialog_PrimaryButtonClick;
                dialog.SecondaryButtonClick += ConnectDialog_SecondaryButtonClick;

                dialog.PrimaryButtonText = "Connect";
                dialog.SecondaryButtonText = "Cancel";
                dialog.IsPrimaryButtonEnabled = true;
                dialog.IsSecondaryButtonEnabled = true;
                dialog.IsPrimaryButtonEnabled = false;

                var textBox = new TextBox();
                textBox.TextChanged += TextBox_TextChanged;
                textBox.Tag = dialog;
                textBox.PlaceholderText = "New device name";

                var panel = new StackPanel();
                panel.Children.Add(textBox);
                dialog.Content = panel;

                await dialog.ShowAsync();
            }

        }

        private void ConnectDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            sender.Hide();
        }

        private void ConnectDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            var ssid = (string)sender.Tag;
            var wifi = vm.NetWorks.Where(x => x.Key == ssid).First().Value;

            var panel = (StackPanel)sender.Content;

            var textBox = (TextBox)panel.Children.First();

            var deviceName = textBox.Text;
            PasswordBox passBox;
            try
            {
                passBox = (PasswordBox)panel.Children.ElementAt(1);
            }
            catch (Exception ex)
            {
                passBox = null;
            }




            if (wifi == null)
            {
                RegisteringDevice(deviceName, ssid);
            }
            else
            {
                if (passBox == null)
                {
                    connect(wifi, deviceName, null);
                }
                else
                {
                    var password = passBox.Password;
                    var credential = new PasswordCredential();
                    credential.Password = password;
                    connect(wifi, deviceName, credential);
                }

            }


        }

        private async void RegisteringDevice(string deviceName, string ssid)
        {
            vm.Progress = Visibility.Visible;
            try
            {
                WiFiConnectionResult result;
                vm.ProgressMessage = "Connecting to network";
                vm.ProgressValue = 75;

                if (vm.PrevWifi.credential == null)
                {
                    result = await firstAdapter.ConnectAsync(vm.PrevWifi.wifi, WiFiReconnectionKind.Manual);
                }
                else
                {
                    result = await firstAdapter.ConnectAsync(vm.PrevWifi.wifi, WiFiReconnectionKind.Manual, vm.PrevWifi.credential);
                }

                await Task.Delay(TimeSpan.FromSeconds(1));
                if (result.ConnectionStatus != WiFiConnectionStatus.Success)
                {
                    throw new Exception("Fail connect to wifi");
                }

                var cachedDevice = vm.CachedDevice.Where(x => x.Key == ssid).First();


                await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);
                var userDeviceVm = new UserDevicesViewModel();
                vm.ProgressMessage = "Registering device " + deviceName;
                vm.ProgressValue = 90;
                await Task.Delay(ConfigHelper.WaitingWifiGetIpTime);

                //code register device

                var deviceDetail = vm.RegisterDevice(cachedDevice.Value, deviceName, ssid);

                //await dialog.ShowAsync();

                //=================
                vm.ProgressMessage = "Complete";
                vm.ProgressValue = 100;

                this.Frame.Navigate(typeof(DeviceDetailView), deviceDetail);
            }
            catch (Exception ex)
            {
                vm.Error = ex.Message;
                firstAdapter.Disconnect();
            }
            vm.Progress = Visibility.Collapsed;
        }

        private async void RefreshWifiList()
        {
            vm.IsLoading = true;
            try
            {
                firstAdapter.Disconnect();
            }
            catch { }


            var access = await WiFiAdapter.RequestAccessAsync();
            if (access != WiFiAccessStatus.Allowed)
            {
                var dialog = new Windows.UI.Popups.MessageDialog("wifi access not allowed", "error");
                await dialog.ShowAsync();
            }
            else
            {

                try
                {
                    var result = await Windows.Devices.WiFi.WiFiAdapter.FindAllAdaptersAsync();
                    //var result = await Windows.Devices.Enumeration.DeviceInformation.FindAllAsync(WiFiAdapter.GetDeviceSelector());
                    if (result.Count >= 1)
                    {
                        try
                        {
                            firstAdapter = result.First();//await WiFiAdapter.FromIdAsync(result[0].Id);
                            await firstAdapter.ScanAsync();
                            var report = firstAdapter.NetworkReport;
                            //foreach (var network in report.AvailableNetworks)
                            //{
                            //    vm.NetWorks.Add(network);
                            //    //Debug.Write(network);
                            //}
                            var availableNetwork = report.AvailableNetworks.Where(x => x.Ssid.ToLower().Contains(ConfigHelper.DeviceName)).ToList();
                            var networks = new Dictionary<string, WiFiAvailableNetwork>();

                            foreach (var cd in vm.CachedDevice)
                            {
                                networks.Add(cd.Key, null);
                            }

                            foreach (var an in availableNetwork)
                            {
                                networks.Add(an.Ssid, an);
                            }


                            vm.NetWorks = networks;
                            vm.NetWorksSsid = networks.Select(x => x.Key).ToList();
                        }
                        catch (Exception ex)
                        {
                            firstAdapter.Disconnect();
                            vm.Error = ex.Message;
                        }

                    }
                    else
                    {
                        var dialog = new Windows.UI.Popups.MessageDialog("No WiFi Adapters detected on this machine", "error");
                        await dialog.ShowAsync();
                        //rootPage.NotifyUser("No WiFi Adapters detected on this machine", NotifyType.ErrorMessage);
                    }
                }
                catch (Exception ex)
                {
                    vm.Error = ex.Message;
                }
            }
            vm.IsLoading = false;
        }
        private async void RefreshWifi(object sender, RoutedEventArgs e)
        {
            RefreshWifiList();
        }
    }

    public class DeviceSetupResponse
    {
        public string id { get; set; }
    }
}
