﻿using SmartGalon.Helper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartGalon.Model.UserDevicesModel;
using Newtonsoft.Json;
using SmartGalon.Model;
using Windows.UI.Core;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Media;

namespace SmartGalon.ViewModel
{
    public class DeviceDetailViewModel : ViewModelBase
    {
        private WebSocketHelper webSocketHelper = new WebSocketHelper();
        public static string PageCache;
        public DeviceDetailViewModel()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }
        }


        #region property

        public Model.UserDevicesModel.Datum data { get; set; }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        private ImageSource galonImage;
        public ImageSource GalonImage
        {
            get { return galonImage; }
            set
            {
                galonImage = value;
                OnPropertyChanged("GalonImage");
            }
        }
        private string types;
        public string Types
        {
            get { return types; }
            set
            {
                types = value;
                OnPropertyChanged("Types");
            }
        }

        private int waterLevel;
        public int WaterLevel
        {
            get { return waterLevel; }
            set
            {
                string image = "";
                if (value > 95)
                {
                    image = "Assets/img_gallon_percentage_100.png";
                }
                else if (value > 85)
                {
                    image = "Assets/img_gallon_percentage_90.png";
                }
                else if (value > 75)
                {
                    image = "Assets/img_gallon_percentage_80.png";
                }
                else if (value > 65)
                {
                    image = "Assets/img_gallon_percentage_70.png";
                }
                else if (value > 55)
                {
                    image = "Assets/img_gallon_percentage_60.png";
                }
                else if (value > 45)
                {
                    image = "Assets/img_gallon_percentage_50.png";
                }
                else if (value > 35)
                {
                    image = "Assets/img_gallon_percentage_40.png";
                }
                else if (value > 25)
                {
                    image = "Assets/img_gallon_percentage_30.png";
                }
                else if (value > 15)
                {
                    image = "Assets/img_gallon_percentage_20.png";
                }
                else if (value > 5)
                {
                    image = "Assets/img_gallon_percentage_10.png";
                }
                else
                {
                    image = "Assets/img_gallon_percentage_0.png";
                }
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                      () =>
                      {
                          // Your UI update code goes here!
                          var imageXaml = new BitmapImage(new Uri("ms-appx:/" + image, UriKind.RelativeOrAbsolute));
                          //imageXaml.Freeze(); // -> to prevent error: "Must create DependencySource on same Thread as the DependencyObject"
                          //galonImage = imageXaml;

                          GalonImage = imageXaml;
                      }
                      );

                waterLevel = value;
                OnPropertyChanged("WaterLevel");
            }
        }

        internal void DisconnectSocket()
        {
            try
            {
                webSocketHelper.Disconnect();
            }
            catch { }

        }

        private string replaceTime;
        public string ReplaceTime
        {
            get { return replaceTime; }
            set
            {
                replaceTime = value;
                OnPropertyChanged("ReplaceTime");
            }
        }

        private long replaceTimeNumber;
        public long ReplaceTimeNumber
        {
            get { return replaceTimeNumber; }
            set
            {
                //System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                //dtDateTime = dtDateTime.AddSeconds(Math.Round((double)value / 1000)).ToLocalTime();

                //var currentDate = DateTime.Now;

                ReplaceTime = "24 Hour";

                replaceTimeNumber = value;
                OnPropertyChanged("ReplaceTimeNumber");
            }
        }


        private string lastReplace;

        public void SetInitialData(Model.UserDevicesModel.Datum data)
        {
            this.data = data;
            if (string.IsNullOrWhiteSpace(PageCache))
            {
                Name = this.data.device.name;
                Types = this.data.device.types;
                WaterLevel = data.attr.waterLevelPercent;
                State = data.attr.state;
                LastReplaceNumber = data.device.lastReplaced;
                ReplaceTimeNumber = data.device.timeToReplace;
            }
            else
            {
                var cache = JsonConvert.DeserializeObject<DeviceDetailViewModel>(PageCache);
                Name = cache.data.device.name;
                Types = cache.data.device.types;
                WaterLevel = cache.data.attr.waterLevelPercent;
                State = cache.data.attr.state;
                LastReplaceNumber = cache.data.device.lastReplaced;
                ReplaceTimeNumber = cache.data.device.timeToReplace; Name = cache.data.device.name;
            }


        }

        public string LastReplace
        {
            get { return lastReplace; }
            set
            {
                lastReplace = value;
                OnPropertyChanged("LastReplace");
            }
        }

        private long lastReplaceNumber;
        public long LastReplaceNumber
        {
            get { return lastReplaceNumber; }
            set
            {

                //var diff = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Math.Round(value / 1000d)).ToLocalTime();
                //var diff = new DateTime(1, 1, 1, 0, 0, 0, 0).AddSeconds(Math.Round(value / 1000d)).ToLocalTime();
                //var currentDate = DateTime.Now;
                //var diff = currentDate = dt;
                //LastReplace= ( (diff.Day!=0)? diff.Day + "days":(diff.Hour!=0)?diff.Hour +"hours":diff.Minute+" minutes")+" ago"  ; 
                LastReplace = "1 days";
                lastReplaceNumber = value;
                OnPropertyChanged("LastReplaceNumber");
            }
        }


        private int state;
        public int State
        {
            get { return state; }
            set
            {
                if (value == 1)
                {
                    StateColor = "Cyan";
                    StateString = "ON";
                }
                else
                {
                    StateColor = "LightGray";
                    StateString = "OFF";
                }
                state = value;
                OnPropertyChanged("State");
            }
        }

        private string stateColor;
        public string StateColor
        {
            get { return stateColor; }
            set
            {
                stateColor = value;
                OnPropertyChanged("StateColor");
            }
        }
        private string stateString;
        public string StateString
        {
            get { return stateString; }
            set
            {
                stateString = value;
                OnPropertyChanged("StateString");
            }
        }

        #endregion



        #region events
        public void SubscribeMetric(string deviceId, string username)
        {
            try
            {
                webSocketHelper.GetSocket.On(WebSocketHelper.matrixEvent, (data) =>
                {
                    ListenToEnvent(data);
                });
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
                Error = ex.Message;
            }

        }
        public void SubscribeGetState(string deviceId, string username)
        {
            try
            {
                webSocketHelper.GetSocket.On(WebSocketHelper.getState, (data) =>
                {
                    ChangeState(data);
                });
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
                Error = ex.Message;
            }

        }

        private void ChangeState(object message)
        {
            var messageString = message.ToString();//JsonConvert.SerializeObject(message);
            var data = JsonConvert.DeserializeObject<StatePayload>(messageString);
            if (data.deviceId!=this.data.device.id)
            {
                return;
            }
            State = data.state;
            this.data.attr.state = data.state;
        }

        private void ListenToEnvent(object message)
        {
            var messageString = JsonConvert.SerializeObject(message);
            var data = JsonConvert.DeserializeObject<GalonMqtttResponseModel>(messageString);

            if (data.deviceId != this.data.device.id)
            {
                return;
            }

            this.data.attr.waterLevelPercent = data.waterLevelPercent;
            this.WaterLevel = data.waterLevelPercent;
            //this.State = data.state == null ? this.state : data.state.Value;

            Debug.Write(message);
        }

        public void Publish(string deviceId, string data, string username)
        {
            try
            {
                var dataObject = JsonConvert.DeserializeObject(data);
                webSocketHelper.GetSocket.Emit(WebSocketHelper.setState, data);
            }
            catch (Exception ex)
            {
                Error = ex.Message;
            }
        }
        
        #endregion
    }
    public class StatePayload
    {
        public string deviceId { get; set; }
        public int state { get; set; }
    }
}
