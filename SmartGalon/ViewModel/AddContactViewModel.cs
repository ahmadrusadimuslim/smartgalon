﻿using Newtonsoft.Json;
using SmartGalon.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.ViewModel
{
    public class AddContactViewModel : ViewModelBase
    {
        public Model.UserDevicesModel.Datum data { get; set; }
        public List<Model.TriggerModel.Action> contactData { get; set; }
        private string notificationType;
        public string NotificationType
        {
            get { return notificationType; }
            set
            {
                notificationType = value;
                OnPropertyChanged("NotificationType");
            }
        }

        private int notificationTypeIndex;
        public int NotificationTypeIndex
        {
            get { return notificationTypeIndex; }
            set
            {
                notificationTypeIndex = value;
                OnPropertyChanged("NotificationTypeIndex");
            }
        }
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        private string types;
        public string Types
        {
            get { return types; }
            set
            {
                types = value;
                OnPropertyChanged("Types");
            }
        }
        private string contactName;
        public string ContactName
        {
            get { return contactName; }
            set
            {
                contactName = value;
                OnPropertyChanged("ContactName");
            }
        }
        private string contactTo;
        public string ContractTo
        {
            get { return contactTo; }
            set
            {
                contactTo = value;
                OnPropertyChanged("ContactTo");
            }
        }
        private string message;

        public string Message
        {
            get { return message ?? "Galon is almost empty, please replace ASAP"; }
            set
            {
                message = value;
                OnPropertyChanged("Message");
            }
        }

        public void LoadData(Model.UserDevicesModel.Datum data, List<Model.TriggerModel.Action> contactData)
        {
            this.types = data.device.types;
            this.name = data.device.name;
            this.data = data;
            this.contactData = contactData;
        }
        public bool SubmitContact()
        {

            var helper = new HttpHelper();
            if (string.IsNullOrWhiteSpace(ContactName) || string.IsNullOrWhiteSpace(Message) || string.IsNullOrWhiteSpace(ContractTo))
            {
                //Error = "Every field must not be empty";
                return false;
            }

            if (notificationTypeIndex==0|| notificationTypeIndex ==1)
            {
                //sms
                long phoneNumber;
                if (ContractTo.Length<3&&message.Substring(0,2)!="+6"&&long.TryParse(ContractTo.Substring(1, ContractTo.Length-1),out phoneNumber))
                {
                    throw new Exception("Please input valid phone Number \nexample: +6281234567890");
                }
            }
            else if (notificationTypeIndex==2)
            {
                //email
                if (!new ValidationHelper().IsValidEmail(ContractTo))
                {
                    throw new Exception("Please input valid email address");
                }
            }

            var type = NotificationTypeIndex == 0 ? "sms" : NotificationTypeIndex == 1 ? "call" : "email";

            var body = new Model.TriggerModel.DatumPost
            {
                user = this.data.account.userId,
                //_id = this.data._id,
                device = this.data.device.id,
                triggerName = "waterlevel",
                threshold = this.data.attr.alertThreshold,
                action = new Model.TriggerModel.ActionPost
                {
                    alias = ContactName,
                    message = Message,
                    to = ContractTo,
                    type =type,
                    //_id = this.data._id
                }
            };
            try
            {
                var data = helper.Post(EndPointHelper.HOST + EndPointHelper.TRIGGER, body);
                var actionString = JsonConvert.SerializeObject(body.action);
                var action = JsonConvert.DeserializeObject<Model.TriggerModel.Action>(actionString);
                if (!data)
                {
                    if (ConfigHelper.Login.username == "testing")
                    {
                       
                        contactData.Add(action);
                        return true;
                    }
                    return false;
                }
                else {


                    contactData.Add(action);
                    return true;
                }
            }
            catch(Exception ex)
            {
                if (ConfigHelper.Login.username == "testing")
                {
                    var actionString = JsonConvert.SerializeObject(body.action);

                    var action = JsonConvert.DeserializeObject<Model.TriggerModel.Action>(actionString);
                    contactData.Add(action);
                    return true;
                }
                return false;
            }
            
        }
    }
}
