﻿using Newtonsoft.Json;
using SmartGalon.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.ViewModel
{
    public class LoginViewModel:ViewModelBase
    {
        public LoginViewModel()
        {
            httpHelper = new HttpHelper();
        }

        #region view property
        private string userName;
        public string Username
        {
            get { return userName; }
            set
            {
                userName = value;
                //OnPropertyChanged("Username");
            }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set
            {
                password = value;
                //OnPropertyChanged("Password");
            }
        }

        #endregion

        #region events Action

        private HttpHelper httpHelper;

        public async Task<Model.LoginModel.RootObject> Authenticate(string username,string password)
        {
            try
            {
                var result = await httpHelper.Auth(username,password);
                var data=JsonConvert.DeserializeObject<Model.LoginModel.RootObject>(result);
                if (data.access_token==null)
                {
                    throw new Exception("login failed");
                }
                data.username = username;
                var fileHelper = new FileHelper();
                fileHelper.SaveAuth(result);
                return data;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                //harus di hapus nanti
                return null;
                //return new Model.LoginModel.RootObject {
                //    access_token = "n9eNJew0bQW3mk0c5I8bhnFrqWO4fOWU3WBANSi3pi1AxF4iB1FgZ6D64yE0P9zrlANWN7eGb2yMP5gUWoEfkZyIsgTkdLc2pFKPHTeOqb2Ldx4jt6ghJ0q41z6GNufD",
                //    refresh_token = "k7iqihyqGvE3cVXdAQA35GGkSJdHWf9izBngHhcY58XBXv6YUYhHkYFgO1N0GsLFIUBFtvlWr2rvvuhowXx3v3SxqD5FxDr82HUTXSWrgmus29OGjJQPawW3CTFIdlsr",
                //    expires_in = new DateTime(2016,09,25,15,22,15),// "2016 -09-25T15:22:15.869Z",
                //    token_type="bearer"
                //};
                //return false;
            }

        }

        #endregion

        #region miscellaneous

        #endregion
    }
}
