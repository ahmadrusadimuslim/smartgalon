﻿using Newtonsoft.Json;
using SmartGalon.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.ViewModel
{
    public class TrigerViewModel : ViewModelBase
    {
        public Model.UserDevicesModel.Datum data { get; set; }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        private string types;
        public string Types
        {
            get { return types; }
            set
            {
                types = value;
                OnPropertyChanged("Types");
            }
        }
        private int waterLevel;
        public int WaterLevel
        {
            get { return waterLevel; }
            set
            {
                waterLevel = value;
                OnPropertyChanged("WaterLevel");
            }
        }

        private int waterTreshold;
        public int WaterTreshold
        {
            get { return waterTreshold; }
            set
            {
                waterTreshold = value;
                OnPropertyChanged("WaterTreshold");
            }
        }
        private List<Model.TriggerModel.Action> contactData;

        public List<Model.TriggerModel.Action> ContactData
        {
            get { return contactData; }
            set
            {
                contactData = value;

                OnPropertyChanged("ContactData");
            }
        }

        public async void SetInitialData(Model.UserDevicesModel.Datum data)
        {
            try
            {
                Name = data.device.name;
                Types = data.device.types;
                WaterLevel = data.attr.waterLevelPercent;
                WaterTreshold = data.attr.alertThreshold;
                this.data = data;
                var httpHelper = new HttpHelper();
                var result = httpHelper.Get(EndPointHelper.HOST + EndPointHelper.TRIGGER_LIST+data.device.id);
                var contacts = JsonConvert.DeserializeObject<Model.TriggerModel.RootObject>(result);
                var temp = contacts.data.Select(x => x.action).ToList();
                int i = 0;
                foreach (var t in temp)
                {
                    t._id = contacts.data.ElementAt(i)._id;
                    i++;
                }
                ContactData = temp;
            }
            catch (Exception ex) {
                Error = ex.Message;
                if (ConfigHelper.Login.username=="testing")
                {
                    ContactData = new List<Model.TriggerModel.Action>
                    {
                        new Model.TriggerModel.Action
                        {
                            alias="testing",
                            message="testing galon is empty",
                            to="testing@test.com",
                            type="Email",
                            _id="testing"
                        }
                    };
                }
            }

        }

        public void SaveChange()
        {
            var httpHelper = new HttpHelper();
            this.data.attr.alertThreshold = waterTreshold;
            var data = new SaveChangesModel { threshold = this.data.attr.alertThreshold, deviceid = this.data.device.id };
            var isSaved = httpHelper.Put(EndPointHelper.HOST + EndPointHelper.TRIGGER, data);

        }
        public class SaveChangesModel
        {
            public int threshold { get; set; }
            public string deviceid { get; set; }
        }
    }
}
