﻿using Newtonsoft.Json;
using SmartGalon.Helper;
using SmartGalon.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.ViewModel
{
    public class UserDevicesViewModel:ViewModelBase
    {
        #region property
        private ObservableCollection<Model.UserDevicesModel.Datum> devices;//=new ObservableCollection<Model.UserDevices.Datum>();
        public ObservableCollection<Model.UserDevicesModel.Datum> Devices
        {
            get { return devices; }
            set
            {
                devices = value;

                OnPropertyChanged("Devices");
            }
        }


        private WebSocketHelper webSocketHelper = new WebSocketHelper();

        #endregion

        public void LoadDevices()
        {
            IsLoading = true;
            var url = EndPointHelper.HOST + EndPointHelper.DEVICES;
            var httpHelper = new HttpHelper();
            try
            {
                var responsestring = httpHelper.Get(url);
                var result = JsonConvert.DeserializeObject<Model.UserDevicesModel.RootObject>(responsestring);
                Devices = new ObservableCollection<Model.UserDevicesModel.Datum> (result.data);
                //foreach (var d in result.data)
                //{
                //    devices.Add(d);
                //}
            }
            catch (Exception ex)
            {
                Error = "Fail to load Devices";
                if (ConfigHelper.Login.username == "testing")
                {
                    Devices = new ObservableCollection<Model.UserDevicesModel.Datum>( new List<Model.UserDevicesModel.Datum> {
                        new Model.UserDevicesModel.Datum
                        {
                            _id="testing",
                            account=new Model.UserDevicesModel.Account {
                                userId="testing",
                                userToken="testing"
                            },
                            device=new Model.UserDevicesModel.Device
                            {
                                id="testing",
                                lastAlert="testing",
                                lastReplaced=0,
                                lastUpdated=0,
                                name="testing",
                                subtypes="testing",
                                timeToReplace=0,
                                types="testing"
                            },
                            attr=new Model.UserDevicesModel.Attr
                            {
                                alertThreshold=30,
                                ldr=0,
                                state=1,
                                waterLevelPercent=30
                            },
                            config=new Model.UserDevicesModel.Config
                            {
                                appPaired=0,
                                emptyThresholds="testing",
                                locationName="testing",
                                sensorOrientation=1
                            }
                        }
                    });
                }
            }
            IsLoading = false;
        }
        public void SubscribeMetric()
        {
            try
            {
                webSocketHelper.GetSocket.On(WebSocketHelper.matrixEvent, (data) =>
                {
                    ListenToEnvent(data);
                });
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
                Error = ex.Message;
            }

        }
        private void ListenToEnvent(object message)
        {
            try
            {
                var messageString = JsonConvert.SerializeObject(message);
                var data = JsonConvert.DeserializeObject<GalonMqtttResponseModel>(messageString);

                var deviceData = devices.Where(x => x.device.id == data.deviceId);
                foreach (var device in deviceData)
                {
                    device.attr.waterLevelPercent = data.waterLevelPercent;
                    //this.State = data.state == null ? this.state : data.state.Value;

                    Debug.Write(message);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
           
            
        }

    }
}
