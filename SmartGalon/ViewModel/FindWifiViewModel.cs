﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartGalon.Model;
using Windows.Devices.WiFi;
using Windows.UI.Xaml;
using SmartGalon.View;
using SmartGalon.Helper;
using SmartGalon.Model.UserDevicesModel;
using Newtonsoft.Json;

namespace SmartGalon.ViewModel
{
    public class FindWifiViewModel : ViewModelBase
    {
        private Dictionary<string,WiFiAvailableNetwork> networks;
        public Dictionary<string, WiFiAvailableNetwork> NetWorks
        {
            get { return networks; }
            set
            {
                networks = value;
                OnPropertyChanged("NetWorks");
            }
        }

        private List<string> networksSsid;
        public List<string> NetWorksSsid
        {
            get { return networksSsid; }
            set
            {
                networksSsid = value;
                OnPropertyChanged("NetWorksSsid");
            }
        }
        private Dictionary<string,string> cachedDevice=new Dictionary<string, string>();
        public Dictionary<string, string> CachedDevice
        {
            get { return cachedDevice; }
            set
            {
                cachedDevice = value;
                OnPropertyChanged("CachedDevice");
            }
        }
        private WifiAccountModel prevWifi;
        public WifiAccountModel PrevWifi
        {
            get { return prevWifi; }
            set
            {
                prevWifi = value;
                OnPropertyChanged("PrevWifi");
            }
        }

        private Visibility progress=Visibility.Collapsed;
        public Visibility Progress
        {
            get { return progress; }
            set
            {
                progress = value;
                OnPropertyChanged("Progress");
            }
        }
        private string progressMessage;
        public string ProgressMessage
        {
            get { return progressMessage; }
            set
            {
                progressMessage = value;
                OnPropertyChanged("ProgressMessage");
            }
        }
        private int progressValue;
        public int ProgressValue
        {
            get { return progressValue; }
            set
            {
                progressValue = value;
                OnPropertyChanged("ProgressValue");
            }
        }

        public Datum  RegisterDevice(string id,string deviceName,string ssid)
        {
            var helper = new HttpHelper();
            var data = new Model.RegisterDeviceModel.RootObject
            {
                account = new Model.RegisterDeviceModel.Account
                {
                    userId = ConfigHelper.Login.username,
                    userToken = ConfigHelper.Login.username//ConfigHelper.Login.access_token
                },
                device = new Model.RegisterDeviceModel.Device
                {
                    name = deviceName,
                    types = "SmartGallon",
                    id=id,
                    description="",
                    subtypes= "SmartGallon",
                    
                    //lastAlert = DateTime.Now.ToString;
                },
                attr=new Model.RegisterDeviceModel.Attr
                {
                    ldr=0,
                    state=0,
                    waterLevelPercent=30
                    
                },
                config=new Model.RegisterDeviceModel.Config
                {
                    ssidName=ssid,
                    locationLat= 107.5844769,
                    locationLong= -6.8768191,
                    locationName="IOT"
                }
            };
            var response= helper.PostWithResult(EndPointHelper.HOST + EndPointHelper.DEVICES, data);
            if (!response.IsSuccessStatusCode)
            {
                return null;
            }
            else {

                //var dataString = JsonConvert.SerializeObject(data);
                var content = response.Content.ReadAsStringAsync().Result;
                var dataDatum= JsonConvert.DeserializeObject<RootObjectSingleData>(content);
                if (dataDatum==null|| dataDatum.data==null)
                {
                    throw new Exception("Fail to register device");
                }
                return dataDatum.data;
            }
        }
    }
}
