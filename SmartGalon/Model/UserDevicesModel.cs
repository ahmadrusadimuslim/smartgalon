﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace SmartGalon.Model.UserDevicesModel
{
    public class Meta : ModelBase
    {
        public string status { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class Device : ModelBase
    {
        public string id { get; set; }
        public string name { get; set; }
        public string types { get; set; }
        public string subtypes { get; set; }
        public object lastUpdated { get; set; }
        public int lastReplaced { get; set; }
        public int timeToReplace { get; set; }
        public string lastAlert { get; set; }
        public string description { get; set; }
    }

    public class Attr : ModelBase
    {
        public int state { get; set; }
        public int waterLevelPercent
        {
            get { return WaterLevelPercent; }
            set
            {
                string image = "";
                if (value > 95)
                {
                    image = "Assets/img_gallon_percentage_100.png";
                }
                else if (value > 85)
                {
                    image = "Assets/img_gallon_percentage_90.png";
                }
                else if (value > 75)
                {
                    image = "Assets/img_gallon_percentage_80.png";
                }
                else if (value > 65)
                {
                    image = "Assets/img_gallon_percentage_70.png";
                }
                else if (value > 55)
                {
                    image = "Assets/img_gallon_percentage_60.png";
                }
                else if (value > 45)
                {
                    image = "Assets/img_gallon_percentage_50.png";
                }
                else if (value > 35)
                {
                    image = "Assets/img_gallon_percentage_40.png";
                }
                else if (value > 25)
                {
                    image = "Assets/img_gallon_percentage_30.png";
                }
                else if (value > 15)
                {
                    image = "Assets/img_gallon_percentage_20.png";
                }
                else if (value > 5)
                {
                    image = "Assets/img_gallon_percentage_10.png";
                }
                else
                {
                    image = "Assets/img_gallon_percentage_0.png";
                }
                Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                      () =>
                      {
                          // Your UI update code goes here!
                          var imageXaml = new BitmapImage(new Uri("ms-appx:/" + image, UriKind.RelativeOrAbsolute));
                          //imageXaml.Freeze(); // -> to prevent error: "Must create DependencySource on same Thread as the DependencyObject"
                          //galonImage = imageXaml;

                          GalonImage = imageXaml;
                      }
                      );


                WaterLevelPercent = value;
                OnPropertyChanged("waterLevelPercent");
            }
        }
        private int WaterLevelPercent;
        public int alertThreshold { get; set; }
        public int? ldr { get; set; }

        private ImageSource galonImage;
        public ImageSource GalonImage
        {
            get { return galonImage; }
            set
            {
                galonImage = value;
                OnPropertyChanged("GalonImage");
            }
        }

    }

    public class Config : ModelBase
    {
        public int appPaired { get; set; }
        public int sensorOrientation { get; set; }
        public string locationName { get; set; }
        public string emptyThresholds { get; set; }
        public string ssidName { get; set; }
        public double? locationLong { get; set; }
        public double? locationLat { get; set; }
    }

    public class Account
    {
        public string userId { get; set; }
        public string userToken { get; set; }
    }

    public class Datum
    {
        public string _id { get; set; }
        public Device device { get; set; }
        public Attr attr { get; set; }
        public Config config { get; set; }
        public Account account { get; set; }
        public int? __v { get; set; }
    }

    public class RootObject
    {
        public Meta meta { get; set; }
        public List<Datum> data { get; set; }
    }

    public class RootObjectSingleData
    {
        public Meta meta { get; set; }
        public Datum data { get; set; }
        public string message { get; set; }
    }
}
