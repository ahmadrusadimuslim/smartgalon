﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.Model.RegisterDeviceModel
{
    
    public class Account
    {
        public string userId { get; set; }
        public string userToken { get; set; }
    }

    public class Attr
    {
        public int state { get; set; }
        public int waterLevelPercent { get; set; }
        public int ldr { get; set; }
    }

    public class Config
    {
        public string ssidName { get; set; }
        public string locationName { get; set; }
        public double locationLong { get; set; }
        public double locationLat { get; set; }
    }

    public class Device
    {
        public string description { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string subtypes { get; set; }
        public string types { get; set; }
    }

    public class RootObject
    {
        public Account account { get; set; }
        public Attr attr { get; set; }
        public Config config { get; set; }
        public Device device { get; set; }
    }
}
