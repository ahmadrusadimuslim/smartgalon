﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.Model.LoginModel
{
    public class RootObject
    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public DateTime expires_in { get; set; }
        public string token_type { get; set; }
        public string username { get; set; }
    }
}
