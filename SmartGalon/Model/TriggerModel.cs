﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.Model.TriggerModel
{
    public class Meta
    {
        public string status { get; set; }
        public int code { get; set; }
        public string message { get; set; }
    }

    public class Action
    {
        public string _id { get; set; }
        public string message { get; set; }
        public string type { get; set; }
        public string alias { get; set; }
        public string to { get; set; }
    }

    public class Datum
    {
        public string _id { get; set; }
        public int threshold { get; set; }
        public string triggerName { get; set; }
        public Action action { get; set; }
        public string user { get; set; }
        public string device { get; set; }
    }

    public class RootObject
    {
        public Meta meta { get; set; }
        public List<Datum> data { get; set; }
    }


    public class DatumPost
    {
        //public string _id { get; set; }
        public int threshold { get; set; }
        public string triggerName { get; set; }
        public ActionPost action { get; set; }
        public string user { get; set; }
        public string device { get; set; }
    }
    public class ActionPost
    {
        //public string _id { get; set; }
        public string message { get; set; }
        public string type { get; set; }
        public string alias { get; set; }
        public string to { get; set; }
    }
}
