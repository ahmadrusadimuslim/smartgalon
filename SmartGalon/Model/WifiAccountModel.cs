﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.WiFi;
using Windows.Security.Credentials;

namespace SmartGalon.Model
{
    public class WifiAccountModel
    {
        public WiFiAvailableNetwork wifi { get; set; }
        public PasswordCredential credential { get; set; }
        public WiFiAdapter adapter { get; set; }
    }
}
