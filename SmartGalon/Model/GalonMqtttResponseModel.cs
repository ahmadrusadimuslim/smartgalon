﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.Model
{
    public class GalonMqtttResponseModel
    {
        public int waterLevelPercent { get; set; }
        public string deviceId { get; set; }
    }
}
