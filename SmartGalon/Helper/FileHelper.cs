﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace SmartGalon.Helper
{
    public class FileHelper
    {
        public async void SaveAuth(string lines)
        {
            var folder = ApplicationData.Current.LocalFolder;

            var path = Path.Combine(folder.Path, "auth.txt");
            try
            {
                System.IO.File.WriteAllText(path, lines);
            }
            catch
            {
                var textFile2 = await folder.CreateFileAsync("auth.txt");
                await FileIO.WriteTextAsync(textFile2, lines);
            }
            
        }
        public async Task<string> GetAuth()
        {
            try
            {
                var folder = ApplicationData.Current.LocalFolder;
                var files = await folder.GetFilesAsync();
                var desiredFile = files.FirstOrDefault(x => x.Name == "auth.txt");
                return await FileIO.ReadTextAsync(desiredFile);
            }
            catch {
                return null;
            }
           
        }
    }
}
