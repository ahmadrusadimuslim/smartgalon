﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.Helper
{
    public class HttpHelper
    {
        private string ClientId = "yUgQqWL7gpuKqVpGf6xrbE6EmGxHJ9ip";
        private string ClientSecret = "ej6WctdyDftkjKACtcjAevQvnL4wUKrX";
        //private string ClientId = "mOlGJA7Rms0u5K2Dijq50ZIuimHuiRzJ";
        //private string ClientSecret = "cPX2ZNGGr89VNTPxwyZUgRPAFp8tsc2u";
        public HttpHelper()
        {
            //put credential here
            
        }
        public string Get(string url)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + ConfigHelper.Login.access_token ?? "");
                var response=client.GetAsync(url).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception();
                }
                return content;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new Exception("Fail Get Data from " + url);
            }
        }

        public string GetNoAuth(string url)
        {
            try
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
                //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + ConfigHelper.Login.access_token ?? "");
                var response = client.GetAsync(url).Result;
                var content = response.Content.ReadAsStringAsync().Result;
                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception();
                }
                return content;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                throw new Exception("Fail Get Data from " + url);
            }
        }

        public bool Post(string url, object body)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + ConfigHelper.Login.access_token ?? "");

            var bodyString = JsonConvert.SerializeObject(body);
            var contentBody=new StringContent(bodyString, Encoding.UTF8, "application/json");

            var response=client.PostAsync(url, contentBody).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            return response.IsSuccessStatusCode;
        }

        public HttpResponseMessage PostWithResult(string url, object body)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + ConfigHelper.Login.access_token ?? "");

            var bodyString = JsonConvert.SerializeObject(body);
            var contentBody = new StringContent(bodyString, Encoding.UTF8, "application/json");

            var response = client.PostAsync(url, contentBody).Result;
            //var content = response.Content.ReadAsStringAsync().Result;
            return response;
        }

        public bool Put(string url, ViewModel.TrigerViewModel.SaveChangesModel body)
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + ConfigHelper.Login.access_token ?? "");

            var bodyString = JsonConvert.SerializeObject(body);
            var contentBody=new StringContent(bodyString, Encoding.UTF8, "application/json");
            var response=client.PutAsync(url, contentBody).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            Debug.Write("");
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
            //return content;
        }
        public bool Delete(string url)
        {
           
            var client = new HttpClient();
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + ConfigHelper.Login.access_token ?? "");
            

            var response = client.DeleteAsync(url).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            Debug.Write("");
            if (response.IsSuccessStatusCode)
            {
                return true;
            }
            else
            {
                return false;
            }
            //return content;
        }
        public async Task<string> Auth(string username,string password)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(EndPointHelper.HOST);
            var request = new HttpRequestMessage(HttpMethod.Post, EndPointHelper.LOGIN);

            var byteArray = new UTF8Encoding().GetBytes(ClientId+":"+ClientSecret);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/json");
            var formData = new List<KeyValuePair<string, string>>();
            formData.Add(new KeyValuePair<string, string>("grant_type", "password"));
            formData.Add(new KeyValuePair<string, string>("username", username));
            formData.Add(new KeyValuePair<string, string>("password", password));

            request.Content = new FormUrlEncodedContent(formData);
            var response = await client.SendAsync(request);
            return response.Content.ReadAsStringAsync().Result;
        }
    }
}
