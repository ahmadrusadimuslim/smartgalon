﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGalon.Helper
{
    public class EndPointHelper
    {
        //public static string HOST = "http://home-x.cloudapp.net:9000/";
        public static string HOST = "http://dycodex.cloudapp.net/";
        //public static string HOST = "http://192.168.1.11:/*9000*//";
        public static string LOGIN = "api/oauth2/signin";
        public static string DEVICES = "api/devices";
        public static string TRIGGER = "api/triggers";
        public static string TRIGGER_LIST = "api/triggers/device/";

        public static string IOT_HUB = "api/iothub/";
    }
}
