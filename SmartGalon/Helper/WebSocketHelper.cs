﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quobject.SocketIoClientDotNet.Client;
using System.Diagnostics;
using Newtonsoft.Json;

namespace SmartGalon.Helper
{
    public class WebSocketHelper
    {
        private bool IsConnected = false;
        private Socket socket;
        private string hskey = "";
        //private string host = "ws://192.168.1.11:9090";
        private const string host = "ws://dycodex.cloudapp.net:9090";

        public const string matrixEvent = "metric";
        public const string setState = "setState";
        public const string getState = "getState";

        public Socket GetSocket
        {
            get { return socket; }
        }

        public WebSocketHelper()
        {
            //var options = new IO.Options();
            //options.Transports = Quobject.Collections.Immutable.ImmutableList<string>.Empty;
            //options.Transports = options.Transports.Add("websocket");
            //options.Transports = options.Transports.Add("flashsocket");
            //options.Transports = options.Transports.Add("htmlfile");
            //options.Transports = options.Transports.Add("xhr-polling");
            //options.Transports = options.Transports.Add("jsonp-polling");

            socket = IO.Socket(host);

            socket.On(Socket.EVENT_CONNECT, () =>
            {
                IsConnected = true;
                Debug.Write("socket connected");
            });
            socket.On("hi", (data) =>
            {
               
            });
            socket.On(Socket.EVENT_CONNECT_ERROR, (data) =>
            {
                Debug.Write(data);
                socket.Disconnect();
            });

            socket.On(Socket.EVENT_DISCONNECT, (data) =>
            {
                IsConnected = false;
                Debug.Write(data);
            });
            socket.On(Socket.EVENT_ERROR, (data) =>
            {
                Debug.Write(data);
                socket.Disconnect();
            });

            Debug.Write("");
        }

        public void Disconnect()
        {
            socket.Disconnect();
        }

        

        //public WebSocketHelper()
        //{
        //    var options = new IO.Options();
        //    options.Transports = Quobject.Collections.Immutable.ImmutableList<string>.Empty;
        //    options.Transports = options.Transports.Add("websocket");
        //    options.Transports = options.Transports.Add("flashsocket");
        //    options.Transports = options.Transports.Add("htmlfile");
        //    options.Transports = options.Transports.Add("xhr-polling");
        //    options.Transports = options.Transports.Add("jsonp-polling");

        //    socket = IO.Socket(host, options);//+hskey 
        //    //socket = IO.Socket(host);

        //    socket.On(Socket.EVENT_CONNECT_ERROR, (data) =>
        //    {
        //        Debug.WriteLine(data);
        //    });

        //    socket.On(Socket.EVENT_CONNECT, (/*data*/) =>
        //    {
        //        socket.Emit("hi",JsonConvert.SerializeObject(new { test = 1, from = "rusadi" }));
        //    });
        //    Test();
        //}
        //public void Test()
        //{

        //    var eventName = "hi";
        //    socket.On(eventName, (data) =>
        //    {
        //        Debug.WriteLine(data);
        //        socket.Disconnect();
        //    });
        //}

    }
}
