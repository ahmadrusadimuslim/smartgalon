# Smart Gallon UWP
Windows 10 app client for Smart Gallon

# Requirment
* Windows 10 Operating System
* Visual Studio 2015 Update 1

## Project Package.appxmanifest capabilities
* Internet (Client)
* Internet (Client & Server)
* Private Network (Client & Server)
* wiFiControl, note: wiFiControl inserted manual in code 
><DeviceCapability Name="wiFiControl" />


# Libraries Nuget Package
SocketIOClientDotNet
Newtonsof.Json

# Configuration files & variables
###[HttpHelper] - configuration for authorization ##
* ClientId
* ClientSecret
 
###[WebSocketHelper] - conconfiguration for web socket
* host
* port
* matrixEvent
* setState
* getState

### [ConfigHelper] - configuration for General Setting ##
* DeviceName
* DeviceSetupUrl 

### [EndPointHelper] - List of Endpoints
* HOST
* LOGIN
* DEVICES
* TRIGGER
* TRIGGER_LIST