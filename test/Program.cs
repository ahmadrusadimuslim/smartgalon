﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.ServiceBus.Messaging;
namespace test
{
    class Program
    {
        static string eventHubName = "gallon-hub.azure-devices.net";
        static string connectionString = "HostName=gallon-hub.azure-devices.net;SharedAccessKeyName=iothubowner;SharedAccessKey=lsAVnUQy3ey4Jdu8p8a7l+7MPbOu1vu49srK0ljd+nI=";
        static void Main(string[] args)
        {
            SendingRandomMessages();
        }
        static void SendingRandomMessages()
        {
            var eventHubClient = EventHubClient.CreateFromConnectionString(connectionString, "devices/gallon01-test01/messages/events");
            
            while (true)
            {
                try
                {
                    var message = Guid.NewGuid().ToString();
                    Console.WriteLine("{0} > Sending message: {1}", DateTime.Now, message);
                    eventHubClient.Send(new EventData(Encoding.UTF8.GetBytes(message)));
                }
                catch (Exception exception)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("{0} > Exception: {1}", DateTime.Now, exception.Message);
                    Console.ResetColor();
                }

                Thread.Sleep(200);
                Console.Read();
            }
        }
    }
}
