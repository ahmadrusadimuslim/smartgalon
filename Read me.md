# Smart Gallon UWP
Windows 10 app client for Smart Gallon

# Reqirement
 - Using windows 10 desktop
 - make sure windows 10 in developer mode
 - user should be administrator
# Installation
 - Right click Add-AppDevPackage.ps1, run with power shell

# Configuration files
* [HttpHelper] - configuration for authorization
* [WebSocketHelper] - conconfiguration for web socket 
* [ConfigHelper] - configuration for General Setting
* [EndPointHelper] - List of Endpoints